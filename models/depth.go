package models

import "time"

type Depth struct {
	Bid     Offer
	Ask     Offer
	Updated time.Time
}
