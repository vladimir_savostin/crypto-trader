package models

//OrderBook - хранит полный стакан
type OrderBook struct {
	Pair     Pair
	Exchange string
	Bids     []Offer
	Asks     []Offer
}

// DepthInstance обощенная структура для bids and asks
type Offer struct {
	Price  float64
	Amount float64
}
