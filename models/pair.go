package models

type Pair struct {
	Name           string
	ActiveCurrency string
	BaseCurrency   string
}
