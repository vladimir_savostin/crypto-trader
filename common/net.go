package common

import (
	"crypto-darvin-b/src/Crypto-Darvin-B/models/constvar"
	"errors"
	"github.com/astaxie/beego/logs"
	jsoniter "github.com/json-iterator/go"
	"io/ioutil"
	"net/http"
)

// отправка неавторизированных запросов напрямую
func SendHTTPGetRequest(url string, jsonDecode bool, result interface{}) (err error) {
	client := http.Client{Timeout: constvar.HTTP_GET_TIMEOUT}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != 200 {
		logs.Warning("HTTP status code: %d\n", res.StatusCode)
		return errors.New("Status code was not 200.")
	}
	contents, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return err
	}
	defer res.Body.Close()
	if jsonDecode {
		err := JSONDecode(contents, &result)
		if err != nil {
			return err
		}
	} else {
		result = &contents
	}
	return nil
}

func JSONDecode(data []byte, to interface{}) error {
	err := jsoniter.Unmarshal(data, &to)
	if err != nil {
		return err
	}

	return nil
}
