package common

import "math"

// отбрасывает все лишние знаки после запятой, до точности precision
func TrimFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(int(num*output)) / output
}
