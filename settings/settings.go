package settings

import (
	"crypto-trader/forms"
	"github.com/astaxie/beego/logs"
	"github.com/tkanos/gonfig"
	"os"
)

var Conf Settings

const confPath = "settings.json"

func init() {
	err := gonfig.GetConf(confPath, &Conf)
	if err != nil {
		logs.Error(err)
		os.Exit(1)
	}
}

type Settings struct {
	Trader    Trader     `json:"trader"`
	Exchanges []Exchange `json:"exchanges"`
	Strategy  Strategy   `json:"strategy"`
}

type Exchange struct {
	Name      string             `json:"name"`
	OrderFee  float64            `json:"order_fee"`
	GetDelay  float32            `json:"get_delay"`
	PostDelay float32            `json:"post_delay"`
	Pairs     []forms.Pair       `json:"pairs"`
	Balance   map[string]float64 `json:"balance"`
}

type Trader struct {
	Balance map[string]float64 `json:"balance"`
}

type Strategy struct {
	Name           string  `json:"name"`
	StopPercent    float64 `json:"stop_percent"`
	MinStopPercent float64 `json:"min_stop_percent"`
	TargetProfit   float64 `json:"target_profit"`
}
