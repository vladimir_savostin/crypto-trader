package main

import (
	"crypto-trader/collector"
	"crypto-trader/constants"
	"crypto-trader/trader"
	"flag"
	"fmt"
	"github.com/astaxie/beego/logs"
	"os"
)

func main() {
	logFileName := flag.String("f", "", "specifies the log file name")
	flag.Parse()
	if *logFileName != "" {
		_, err := os.Create(*logFileName)
		if err != nil {
			logs.Error(err)
			os.Exit(3)
		}
		err = logs.SetLogger(logs.AdapterFile,
			fmt.Sprintf("{\"filename\":\"%s\"}", *logFileName))
		if err != nil {
			logs.Error(err)
			os.Exit(3)
		}
	}

	collector := collector.NewCollector()
	trader := trader.NewTrader()
	for _, exchange := range trader.Exchanges {
		collector.GetOrderBooks(exchange.GetPairs(), exchange.GetOrderBooks)
	}
	for book := range collector.Books {
		activeBalance := trader.Exchanges[book.Exchange].GetBalance(book.Pair.ActiveCurrency)
		baseBalance := trader.Exchanges[book.Exchange].GetBalance(book.Pair.BaseCurrency)
		logs.Info(book.Asks[0].Price,
			book.Bids[0].Price,
			trader.Strategies[book.Exchange][book.Pair.Name],
			activeBalance,
			baseBalance)
		command := trader.Strategies[book.Exchange][book.Pair.Name].CheckOrderBook(book)
		switch command {
		case constants.CommandBuy:
			err := trader.Exchanges[book.Exchange].PlaceOrder(book, constants.CommandBuy)
			if err != nil {
				logs.Error(err)
			}
			logs.Warn(constants.CommandBuy, book.Pair.Name, book.Asks[0].Price)
			trader.Strategies[book.Exchange][book.Pair.Name].Reset(constants.SideActive, trader.Exchanges[book.Exchange])
		case constants.CommandSell:
			err := trader.Exchanges[book.Exchange].PlaceOrder(book, constants.CommandSell)
			if err != nil {
				logs.Error(err)
			}
			logs.Warn(constants.CommandSell, book.Pair.Name, book.Bids[0].Price)
			trader.Strategies[book.Exchange][book.Pair.Name].Reset(constants.SideBase, trader.Exchanges[book.Exchange])
		case constants.CommandWait:
		}
	}
}
