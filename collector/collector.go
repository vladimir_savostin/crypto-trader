package collector

import (
	"crypto-trader/models"
)

type Collector struct {
	Books chan models.OrderBook
}

func NewCollector() Collector {
	var c Collector
	c.Books = make(chan models.OrderBook)
	return c
}

func (c Collector) GetOrderBooks(pairs []models.Pair, f func(pair models.Pair, ch chan models.OrderBook)) {
	for _, pair := range pairs {
		f(pair, c.Books)
	}
}
