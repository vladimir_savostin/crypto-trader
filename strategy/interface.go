package strategy

import (
	"crypto-trader/exchanges"
	"crypto-trader/models"
)

type Strategy interface {
	Setup(pair models.Pair, exchange exchanges.Exchanger) error
	CheckOrderBook(book models.OrderBook) (command string)
	Reset(side string, ex exchanges.Exchanger)
}

func CreateFromName(name string) Strategy {
	switch name {
	case SimpleName:
		return new(SimpleStrategy)
	case TakeProfitName:
		return new(TakeProfitStrategy)
	default:
		return nil
	}
}
