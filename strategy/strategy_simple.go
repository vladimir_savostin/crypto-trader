package strategy

import (
	"crypto-trader/constants"
	"crypto-trader/exchanges"
	"crypto-trader/models"
	"crypto-trader/settings"
	"math"
)

const SimpleName = "simple"

type SimpleStrategy struct {
	stopPrice   float64 //цена при которой надо выходить из позиции
	stopPercent float64 //расстояние на которое отсупает stopPrice от текущей цены, выраженное в процентах
	currentSide string  //текущая позиция, в которой мы находимся (active/base)
}

func (s *SimpleStrategy) Setup(pair models.Pair, exchange exchanges.Exchanger) error {
	s.stopPercent = settings.Conf.Strategy.StopPercent
	activeBalance := exchange.GetBalance(pair.ActiveCurrency)
	baseBalance := exchange.GetBalance(pair.BaseCurrency)
	if activeBalance > baseBalance {
		s.Reset(constants.SideActive, nil)
	} else {
		s.Reset(constants.SideBase, nil)
	}
	return nil
}

func (s *SimpleStrategy) SetStopPrice(price float64) {
	s.stopPrice = price
}
func (s *SimpleStrategy) CheckOrderBook(book models.OrderBook) (command string) {
	var currentPrice float64
	if s.currentSide == constants.SideActive {
		currentPrice = book.Bids[0].Price
		if currentPrice-s.stopPrice > currentPrice*s.stopPercent/100 {
			s.SetStopPrice(currentPrice * (1 - s.stopPercent/100))
		}
		if currentPrice <= s.stopPrice {
			return constants.CommandSell
		}
	}
	if s.currentSide == constants.SideBase {
		currentPrice = book.Asks[0].Price
		if -1*(currentPrice-s.stopPrice) > currentPrice*s.stopPercent/100 {
			s.SetStopPrice(currentPrice * (1 + s.stopPercent/100))
		}
		if currentPrice >= s.stopPrice {
			return constants.CommandBuy
		}
	}
	return constants.CommandWait
}

func (s *SimpleStrategy) Reset(side string, exchanger exchanges.Exchanger) {
	s.currentSide = side
	switch side {
	case constants.SideActive:
		s.stopPrice = 0
	case constants.SideBase:
		s.stopPrice = math.Inf(1)
	}

}
