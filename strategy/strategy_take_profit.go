package strategy

import (
	"crypto-trader/constants"
	"crypto-trader/exchanges"
	"crypto-trader/models"
	"crypto-trader/settings"
	"github.com/astaxie/beego/logs"
	"math"
	"time"
)

const TakeProfitName = "takeProfit"

//TakeProfitStrategy - стратегия такая же как и simple,
//но как только цена доходит до безубытка, стоп-цена пододвигается ближе к этому уровню
type TakeProfitStrategy struct {
	pair           models.Pair //имя пары
	stopPrice      float64     //цена при которой надо выходить из позиции
	stopPercent    float64     //расстояние на которое отсупает stopPrice от текущей цены, выраженное в процентах
	minStopPercent float64     //minStopPercent - используется вместо stopPercent при достиении профит-цены
	currentSide    string      //текущая позиция, в которой мы находимся (active/base)
	profitPrice    float64     //цена, достигнув которой мы однозначно закроем позицию без убытка
	//targetProfit - требуемый профит в каждой сделке
	//(если цена достигает значения, обеспечивающего такой профит,
	// то стоп-приказ подтягивается к этому значению)
	targetProfit float64
	orderFee     float64 //комиссия за ордер
}

func (s *TakeProfitStrategy) Setup(pair models.Pair, exchange exchanges.Exchanger) error {
	s.pair = pair
	s.targetProfit = settings.Conf.Strategy.TargetProfit
	s.orderFee = exchange.GetOrderFee()
	s.stopPercent = settings.Conf.Strategy.StopPercent
	s.minStopPercent = settings.Conf.Strategy.MinStopPercent
	activeBalance := exchange.GetBalance(pair.ActiveCurrency)
	baseBalance := exchange.GetBalance(pair.BaseCurrency)
	if activeBalance > baseBalance {
		s.Reset(constants.SideActive, exchange)
	} else {
		s.Reset(constants.SideBase, exchange)
	}
	return nil
}

func (s *TakeProfitStrategy) SetStopPrice(price float64) {
	s.stopPrice = math.Floor(price*100) / 100
}
func (s *TakeProfitStrategy) CheckOrderBook(book models.OrderBook) (command string) {
	var currentPrice float64
	if s.currentSide == constants.SideActive {
		currentPrice = book.Bids[0].Price
		if currentPrice-s.stopPrice > currentPrice*s.stopPercent/100 {
			s.SetStopPrice(currentPrice * (1 - s.stopPercent/100))
		}
		if currentPrice <= s.stopPrice {
			return constants.CommandSell
		}
		if currentPrice >= s.profitPrice {
			logs.Debug("reached profit price")
			s.SetStopPrice(currentPrice * (1 - s.minStopPercent/100))
		}
	}
	if s.currentSide == constants.SideBase {
		currentPrice = book.Asks[0].Price
		if -1*(currentPrice-s.stopPrice) > currentPrice*s.stopPercent/100 {
			s.SetStopPrice(currentPrice * (1 + s.stopPercent/100))
		}
		if currentPrice >= s.stopPrice {
			return constants.CommandBuy
		}
		if currentPrice <= s.profitPrice {
			logs.Debug("reached profit price")
			s.SetStopPrice(currentPrice * (1 + s.minStopPercent/100))
		}
	}
	return constants.CommandWait
}

func (s *TakeProfitStrategy) Reset(side string, ex exchanges.Exchanger) {
	s.currentSide = side
	depth := ex.GetDepth(s.pair)
	if time.Since(depth.Updated) > time.Millisecond*1500 {
		logs.Error("depth for %s is outdated", s.pair.Name)
	}
	switch side {
	case constants.SideActive:
		s.stopPrice = math.Inf(-1)
		//профит-цена должна быть выше текущего уровня с учетом требуемого профита и комиссионных издержек
		s.profitPrice = depth.Ask.Price * (1 + (s.targetProfit+s.orderFee)/100)
	case constants.SideBase:
		s.stopPrice = math.Inf(1)
		//профит-цена должна быть ниже текущего уровня с учетом требуемого профита и комиссионных издержек
		s.profitPrice = depth.Bid.Price * (1 - (s.targetProfit+s.orderFee)/100)
	}
	s.profitPrice = math.Floor(s.profitPrice*100) / 100
	s.targetProfit = math.Floor(s.targetProfit*100) / 100
}
