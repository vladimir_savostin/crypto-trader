package main

import (
	"crypto-trader/collector"
	"crypto-trader/common"
	"crypto-trader/constants"
	"crypto-trader/settings"
	"crypto-trader/trader"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/logs"
	"sync"
)

const exchange = "binance"
const strategy = "simple"

const (
	minStopPercent    = 0.01
	maxStopPercent    = 10
	minMinStopPercent = 0.01
	maxMinStopPercent = 0.1
	percentStep       = 0.02
	minTargetProfit   = 0.0
	maxTargetProfit   = 2.0
	profitStep        = 0.02
)

func main() {
	//err := logs.SetLogger(logs.AdapterFile,
	//	fmt.Sprintf("{\"filename\":\"%s\"}", "tester.log"))
	//if err != nil {
	//	logs.Error(err)
	//	os.Exit(3)
	//}
	logs.SetLevel(logs.LevelInfo)
	//создаем один коллектор
	collector := collector.NewCollector()
	logs.Info(collector)
	traders := initTraders(strategy)
	logs.Info("ALL TRADERS WAS CREATED")
	logs.Info("%v traders initiated", len(traders))
	exchange := traders[0].Exchanges[exchange]
	logs.Info("start get order books")
	collector.GetOrderBooks(exchange.GetPairs(), exchange.GetOrderBooks)
	for book := range collector.Books {
		wg := new(sync.WaitGroup)
		for _, v := range traders {
			currentTrader := v
			wg.Add(1)
			go func() {
				defer wg.Done()
				command := currentTrader.Strategies[book.Exchange][book.Pair.Name].CheckOrderBook(book)
				switch command {
				case constants.CommandBuy:
					err := currentTrader.Exchanges[book.Exchange].PlaceOrder(book, constants.CommandBuy)
					if err != nil {
						logs.Error(err)
					}
					currentTrader.Strategies[book.Exchange][book.Pair.Name].Reset(constants.SideActive, currentTrader.Exchanges[book.Exchange])
				case constants.CommandSell:
					err := currentTrader.Exchanges[book.Exchange].PlaceOrder(book, constants.CommandSell)
					if err != nil {
						logs.Error(err)
					}
					currentTrader.Strategies[book.Exchange][book.Pair.Name].Reset(constants.SideBase, currentTrader.Exchanges[book.Exchange])
				case constants.CommandWait:
				}
			}()
		}
		logs.Info(book)
		wg.Wait()
	}
}

func initTraders(strategy string) []trader.Trader {
	var traders []trader.Trader
	//создаем разные конфигурации и для каждой создаем трейдера
	for stopPercent := minStopPercent; stopPercent <= maxStopPercent; stopPercent += percentStep {
		for minStopPercent := minMinStopPercent; minStopPercent <= maxMinStopPercent; minStopPercent += percentStep {
			for targetProfit := minTargetProfit; targetProfit <= maxTargetProfit; targetProfit += profitStep {
				//создаем очередную конфигу
				conf := fmt.Sprintf(testConfig,
					strategy, stopPercent, minStopPercent, 1)
				//парсим ее в глобальную конфигу
				err := json.Unmarshal([]byte(conf), &settings.Conf)
				if err != nil {
					panic(err)
				}
				settings.Conf.Strategy.MinStopPercent = common.TrimFixed(
					settings.Conf.Strategy.MinStopPercent, 5)
				settings.Conf.Strategy.StopPercent = common.TrimFixed(
					settings.Conf.Strategy.StopPercent, 5)
				settings.Conf.Strategy.TargetProfit = common.TrimFixed(
					settings.Conf.Strategy.TargetProfit, 5)
				logs.Info("%+v", settings.Conf.Strategy)
				traders = append(traders, trader.NewTrader())
			}
		}
	}
	return traders
}

//test configuration with placeholders for variadic params
const testConfig = `
{
 "trader": {
  },
  "strategy": {
    "name": "%s",
    "stop_percent": %v,
    "min_stop_percent": %v,
    "target_profit": %v
  },
  "exchanges": [
    {
      "name": "binance",
      "order_fee": 0.1,
      "get_delay": 1000,
      "post_delay": 10000,
      "pairs": [
        {
          "name": "btcusdt",
          "active_currency": "btc",
          "base_currency": "usdt"
        }
      ],
      "balance": {
        "btc": 0.1,
        "usdt": 0
      }
    }
  ]
}
`
