package constants

const (
	SideActive  = "active"
	SideBase    = "base"
	CommandBuy  = "buy"
	CommandSell = "sell"
	CommandWait = "wait"
)
