package forms

type Pair struct {
	Name           string
	ActiveCurrency string `json:"active_currency"`
	BaseCurrency   string `json:"base_currency"`
}
