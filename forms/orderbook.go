package forms

import jsoniter "github.com/json-iterator/go"

type OrderBook struct {
	Bids [][]jsoniter.Number
	Asks [][]jsoniter.Number
}
