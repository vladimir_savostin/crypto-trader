package trader

import (
	"crypto-trader/exchanges"
	"crypto-trader/models"
	"crypto-trader/settings"
	"crypto-trader/strategy"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/copier"
)

type Trader struct {
	Exchanges  map[string]exchanges.Exchanger          //exchangeName-exchange
	Strategies map[string]map[string]strategy.Strategy //exchangeName-pair-strategy
}

func NewTrader() Trader {
	var t Trader
	t.Strategies = make(map[string]map[string]strategy.Strategy)
	t.Exchanges = make(map[string]exchanges.Exchanger)
	for _, data := range settings.Conf.Exchanges {
		newExchange := exchanges.CreateFromName(data.Name)
		if newExchange == nil {
			logs.Error("could not create exchange %s", data.Name)
			continue
		}
		newExchange.Setup(data)
		t.Exchanges[data.Name] = newExchange
		for _, v := range data.Pairs {
			var pair models.Pair
			err := copier.Copy(&pair, &v)
			if err != nil {
				panic(err)
			}
			t.Strategies[data.Name] = make(map[string]strategy.Strategy)
			t.Strategies[data.Name][v.Name] = strategy.CreateFromName(settings.Conf.Strategy.Name)
			err = t.Strategies[data.Name][pair.Name].Setup(pair, t.Exchanges[data.Name])
			if err != nil {
				panic(err)
			}
		}
	}
	return t
}
