package exchanges

import (
	"crypto-trader/exchanges/binance"
	"crypto-trader/models"
	"crypto-trader/settings"
)

// описание интерфейса для подключения биржи к боту
type Exchanger interface {
	//Setup - функция первончальной инициализации бирж
	Setup(settings.Exchange)

	//GetOrderBooks - получение ордербуков и отправка их в канал
	GetOrderBooks(pair models.Pair, books chan models.OrderBook)

	//GetDepth - получить последний актуальный верх стакана
	GetDepth(pair models.Pair) models.Depth

	//GetBalance() - получить баланс биржи (из памяти)
	GetBalance(currency string) float64

	//GetPairs() - получить список пар
	GetPairs() []models.Pair

	//PlaceOrder - выставить ордер
	PlaceOrder(book models.OrderBook, side string) error

	//GetOrderFee - получить размер комиссии за ордер
	GetOrderFee() float64
}

func CreateFromName(name string) Exchanger {
	switch name {
	case "binance":
		return new(binance.Binance)
	default:
		return nil
	}
}
