// описание базовой структуры биржи и общих методов

package exchange

import (
	"crypto-trader/constants"
	"crypto-trader/models"
	"crypto-trader/settings"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/copier"
	"sync"
	"sync/atomic"
	"time"
)

type Exchange struct {
	Name         string
	Pairs        []models.Pair
	Depths       map[string]models.Depth
	balance      map[string]float64
	getDelay     time.Duration    // промежутки между запросами в мс
	postDelay    time.Duration    // промежутки между запросами в мс
	getThrottle  <-chan time.Time // "Дроссель" - ограничивает частоту выполнения запросов
	postThrottle <-chan time.Time // "Дроссель" - ограничивает частоту выполнения запросов
	errorCount   int32            // счетчик количества ошибок
	orderFee     float64          //комиссия за ордер в процентах

	mu *sync.Mutex
}

// Первоначальная инициализация биржи
func (ex *Exchange) Setup(exchangeConf settings.Exchange) {
	ex.Name = exchangeConf.Name
	//копируем пары
	err := copier.Copy(&ex.Pairs, &exchangeConf.Pairs)
	if err != nil {
		panic(err)
	}
	//копируем баланс
	ex.balance = make(map[string]float64)
	for currency, balance := range exchangeConf.Balance {
		ex.balance[currency] = balance
	}
	//инициализируем карту стаканов
	ex.Depths = make(map[string]models.Depth)
	ex.orderFee = exchangeConf.OrderFee
	ex.getDelay = time.Duration(exchangeConf.GetDelay)
	ex.postDelay = time.Duration(exchangeConf.PostDelay)
	// запуск дросселей
	ex.getThrottle = time.Tick(ex.getDelay * time.Millisecond)
	ex.postThrottle = time.Tick(ex.postDelay * time.Millisecond)
	ex.errorCount = 0
	//инициализация мьютекса
	ex.mu = &sync.Mutex{}
}

func (ex *Exchange) GetDepth(pair models.Pair) models.Depth {
	ex.mu.Lock()
	defer ex.mu.Unlock()
	return ex.Depths[pair.Name]
}

func (ex *Exchange) SetDepth(pair models.Pair, depth models.Depth) {
	ex.mu.Lock()
	defer ex.mu.Unlock()
	depth.Updated = time.Now()
	ex.Depths[pair.Name] = depth
}

func (ex *Exchange) GetOrderFee() float64 {
	return ex.orderFee
}

func (ex *Exchange) GetBalance(currency string) float64 {
	return ex.balance[currency]
}

func (ex *Exchange) GetPairs() []models.Pair {
	return ex.Pairs
}

func (ex *Exchange) PlaceOrder(book models.OrderBook, side string) error {
	logs.Info("%p, %p", ex, ex.balance)
	switch side {
	case constants.CommandBuy:
		amount := ex.balance[book.Pair.BaseCurrency] / book.Asks[0].Price
		ex.balance[book.Pair.ActiveCurrency] += amount * (1 - ex.orderFee/100)
		ex.balance[book.Pair.BaseCurrency] = 0
	case constants.CommandSell:
		amount := ex.balance[book.Pair.ActiveCurrency] * book.Bids[0].Price
		ex.balance[book.Pair.ActiveCurrency] = 0
		ex.balance[book.Pair.BaseCurrency] += amount * (1 - ex.orderFee/100)
	}
	return nil
}

// установка дросселя Get
func (ex *Exchange) SetGThrottle(t <-chan time.Time) {
	ex.getThrottle = t
}

// установка дросселя Post
func (ex *Exchange) SetPThrottle(t <-chan time.Time) {
	ex.postThrottle = t
}

// получение счетчика ошибок проверки статуса
func (ex *Exchange) GetErrorCount() int32 {
	return atomic.LoadInt32(&ex.errorCount)
}

// установление счетсчика ошибок
func (ex *Exchange) SetErrorCount(count int32) {
	atomic.StoreInt32(&ex.errorCount, count)
}
