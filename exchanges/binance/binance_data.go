package binance

import (
	exchange "crypto-trader/exchanges/common-exchange"
	jsoniter "github.com/json-iterator/go"
	"time"
)

const (
	API_URL       = "https://api.binance.com"
	EXCHANGE_INFO = "api/v1/exchangeInfo"
	ORDER_BOOK    = "api/v3/ticker/bookTicker" //получение лучших цен по всем парам
	ACC_INFO      = "api/v3/account"
	ACTIVE_ORDERS = "api/v3/openOrders"
	ORDER         = "api/v3/order"
	SYSTEM_STATUS = "wapi/v3/systemStatus.html"
)

// определяем тип - Binance
type Binance struct {
	exchange.Exchange
	WsLastKeepAlive time.Time
}

// дополнительные поля при возникновении ошибки
type ResponseError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type ExchangeInfo struct {
	Timezone   string `json:"timezone"`
	ServerTime int64  `json:"serverTime"`
	RateLimits []struct {
		RateLimitType string `json:"rateLimitType"`
		Interval      string `json:"interval"`
		Limit         int    `json:"limit"`
	} `json:"rateLimits"`
	ExchangeFilters []interface{} `json:"exchangeFilters"`
	Symbols         []struct {
		Symbol             string   `json:"symbol"`
		Status             string   `json:"status"`
		BaseAsset          string   `json:"baseAsset"`
		BaseAssetPrecision int      `json:"baseAssetPrecision"`
		QuoteAsset         string   `json:"quoteAsset"`
		QuotePrecision     int      `json:"quotePrecision"`
		OrderTypes         []string `json:"orderTypes"`
		IcebergAllowed     bool     `json:"icebergAllowed"`
		Filters            []struct {
			FilterType  string  `json:"filterType"`
			MinPrice    float64 `json:"minPrice,omitempty,string"`
			MaxPrice    float64 `json:"maxPrice,omitempty,string"`
			TickSize    float64 `json:"tickSize,omitempty,string"`
			MinQty      float64 `json:"minQty,omitempty,string"`
			MaxQty      float64 `json:"maxQty,omitempty,string"`
			StepSize    float64 `json:"stepSize,omitempty,string"`
			MinNotional float64 `json:"minNotional,omitempty,string"`
		} `json:"filters"`
	} `json:"symbols"`
}

type BinanceOrderBook []struct {
	Symbol   string  `json:"symbol"`
	BidPrice float64 `json:"bidPrice,string"`
	BidQty   float64 `json:"bidQty,string"`
	AskPrice float64 `json:"askPrice,string"`
	AskQty   float64 `json:"askQty,string"`
}

type Depth struct {
	Bids [][]jsoniter.Number `json:"bids"` //[][0]- price, [][1] - amount
	Asks [][]jsoniter.Number `json:"asks"` //[][0]- price, [][1] - amount
}

type BinanceAccountInfo struct {
	MakerCommission  int  `json:"makerCommission"`
	TakerCommission  int  `json:"takerCommission"`
	BuyerCommission  int  `json:"buyerCommission"`
	SellerCommission int  `json:"sellerCommission"`
	CanTrade         bool `json:"canTrade"`
	CanWithdraw      bool `json:"canWithdraw"`
	CanDeposit       bool `json:"canDeposit"`
	UpdateTime       int  `json:"updateTime"`
	Balances         []struct {
		Asset  string  `json:"asset"`
		Free   float64 `json:"free,string"`
		Locked float64 `json:"locked,string"`
	} `json:"balances"`
}

type BinanceActiveOrders []struct {
	Symbol        string `json:"symbol"`
	OrderID       int    `json:"orderId"`
	ClientOrderID string `json:"clientOrderId"`
	Price         string `json:"price"`
	OrigQty       string `json:"origQty"`
	ExecutedQty   string `json:"executedQty"`
	Status        string `json:"status"`
	TimeInForce   string `json:"timeInForce"`
	Type          string `json:"type"`
	Side          string `json:"side"`
	StopPrice     string `json:"stopPrice"`
	IcebergQty    string `json:"icebergQty"`
	Time          int64  `json:"time"`
	IsWorking     bool   `json:"isWorking"`
}

type BinanceNewOrder struct {
	Symbol        string `json:"symbol"`
	OrderID       int    `json:"orderId"`
	ClientOrderID string `json:"clientOrderId"`
	TransactTime  int64  `json:"transactTime"`
}

type BinanceCancelOrder struct {
	Symbol            string `json:"symbol"`
	OrigClientOrderID string `json:"origClientOrderId"`
	OrderID           int    `json:"orderId"`
	ClientOrderID     string `json:"clientOrderId"`
}

type BinanceSystemStatus struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
}
