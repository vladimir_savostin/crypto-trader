package binance

import (
	"crypto-trader/common"
	"crypto-trader/forms"
	"crypto-trader/models"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"net/url"
)

func (ex *Binance) GetOrderBooks(pair models.Pair, ch chan models.OrderBook) {
	u := url.URL{Scheme: "wss", Host: "stream.binance.com:9443", Path: fmt.Sprintf("ws/%s@depth%v", pair.Name, 5)}
	logs.Info("connecting to %s", u.String())
	dialer := websocket.DefaultDialer
	dialer.ReadBufferSize = 1024
	c, r, err := websocket.DefaultDialer.Dial(u.String(), nil)
	body, err := ioutil.ReadAll(r.Body)
	logs.Info("%s", body)
	if err != nil {
		panic(err)
	}
	go func() {
		defer c.Close()
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				logs.Error(err)
				logs.Info("start to reconnect to socket")
				err := c.Close()
				common.CheckError(err)
				c, r, err = websocket.DefaultDialer.Dial(u.String(), nil)
				body, err := ioutil.ReadAll(r.Body)
				logs.Info("%s", body)
				common.CheckError(err)
			}
			orderBook := forms.OrderBook{}
			err = json.Unmarshal(message, &orderBook)
			if err != nil {
				logs.Error(err)
			}
			bidPrice, _ := orderBook.Bids[0][0].Float64()
			bidAmount, _ := orderBook.Bids[0][1].Float64()
			askPrice, _ := orderBook.Asks[0][0].Float64()
			askAmount, _ := orderBook.Asks[0][1].Float64()
			ex.SetDepth(pair,
				models.Depth{
					Ask: models.Offer{Price: askPrice, Amount: askAmount},
					Bid: models.Offer{Price: bidPrice, Amount: bidAmount}})
			ch <- models.OrderBook{
				Pair:     pair,
				Exchange: ex.Name,
				Bids: []models.Offer{
					{Price: bidPrice, Amount: bidAmount},
				},
				Asks: []models.Offer{
					{Price: askPrice, Amount: askAmount},
				},
			}
		}
	}()

}
