package binance

// функция инициализации уникальных полей для биржи (например, ClientID). Запускается после функции Setup
func (ex *Binance) SetupUniq() error {
	return nil
}

//// получеие информации о валютах
//// на выходе валюты в нужном формате
//// ключ - esymbol
//func (ex *Binance) GetCurrencies() (map[string]dbmodels.ExCurrency, error) {
//	response := struct {
//		ResponseError
//		ExchangeInfo
//	}{}
//	req := fmt.Sprintf("%s/%s", API_URL, EXCHANGE_INFO)
//
//	// ждем таймер
//	<-ex.getThrottle
//	err := common.SendHTTPGetRequest(req, true, &response)
//	if err != nil {
//		return nil, err
//	}
//
//	// если ответ отрицательный
//	if response.Code != 0 || response.Msg != "" {
//		return nil, errors.New(response.Msg)
//	}
//
//	// формируем список валют
//	currencies := make(map[string]struct{})
//	for _, v := range response.Symbols {
//		if v.Status == "TRADING" {
//			currencies[v.BaseAsset] = struct{}{}
//			currencies[v.QuoteAsset] = struct{}{}
//		}
//
//	}
//
//	// формируем вывод функции в фиксированное описание валюты
//	ret := map[string]dbmodels.ExCurrency{}
//	for k, _ := range currencies {
//		ret[k] = dbmodels.ExCurrency{
//			Name:          k,
//			Exchange:      constvar.BINANCE,
//			ESymbol:       k,
//			BaseAddress:   "",
//			WdFee:         constvar.DEFAULT_WD_DEP_FEE,
//			DepFee:        constvar.DEFAULT_WD_DEP_FEE,
//			AdminDisabled: false,
//			TempDisabled:  false,
//			CauseDisabled: constvar.CauseDisabled_not,
//			Notice:        "",
//		}
//	}
//
//	return ret, nil
//}
//
//// получение списка пар биржи
//func (ex *Binance) GetPairs() (map[string]dbmodels.ExPair, error) {
//	response := struct {
//		ResponseError
//		ExchangeInfo
//	}{}
//	req := fmt.Sprintf("%s/%s", API_URL, EXCHANGE_INFO)
//
//	// ждем таймер
//	<-ex.getThrottle
//	err := common.SendHTTPGetRequest(req, true, &response)
//	if err != nil {
//		return nil, err
//	}
//
//	// если ответ отрицательный
//	if response.Code != 0 || response.Msg != "" {
//		return nil, errors.New(response.Msg)
//	}
//
//	pairs := map[string]dbmodels.ExPair{}
//	for _, v := range response.Symbols {
//		if v.Status == "TRADING" {
//			priceStep := 0.
//			lotSize := 0.
//			minPrice := 0.
//			minOrderSize := 0.
//			for _, f := range v.Filters {
//				if f.FilterType == "PRICE_FILTER" {
//					priceStep = f.TickSize
//					minPrice = f.MinPrice
//				}
//				if f.FilterType == "LOT_SIZE" {
//					lotSize = f.StepSize
//					minOrderSize = f.MinQty
//				}
//			}
//			pairs[v.Symbol] = dbmodels.ExPair{
//				ESymbol:      v.Symbol,
//				PriceStep:    priceStep,
//				LotSize:      lotSize,
//				Exchange:     constvar.BINANCE,
//				Precision:    common.DecimalPlace(lotSize),
//				MinOrderSize: minOrderSize,
//				MinPrice:     minPrice,
//			}
//		}
//
//	}
//	return pairs, err
//}
//
//// "миксование пары"
//// получение пары из вух валют по правилам биржи
//func (ex *Binance) MixPair(active, base string) string {
//	return active + base
//}
//
//func (ex *Binance) GetDepth(pair models.Pair) (models.OrderBook, error) {
//	response := BinanceOrderBook{}
//	req := fmt.Sprintf("%s/%s", API_URL, ORDER_BOOK)
//	err := common.SendHTTPGetRequest(req, true, &response)
//	if err != nil {
//		return models.OrderBook{}, err
//	}
//	for _, book := range response {
//		if strings.ToUpper(book.Symbol) == strings.ToUpper(pair.Name) {
//			return models.OrderBook{
//				Pair:     pair,
//				Exchange: ex.Name,
//				Asks: []models.Offer{
//					{
//						Price:  book.AskPrice,
//						Amount: book.AskQty,
//					},
//				},
//				Bids: []models.Offer{
//					{
//						Price:  book.BidPrice,
//						Amount: book.BidQty,
//					},
//				},
//			}, nil
//		}
//	}
//	return models.OrderBook{}, fmt.Errorf("not found depth for %s on %s", pair.Name, ex.Name)
//}

//
//// получение баланса
//// возвращает карту валют с ключами - именами ESymbol, даже если валюта там одна
//// на выходе ключи в ecurr
//func (ex *Binance) GetBalance(ecurr string, user string) (map[string]memodels.Balance, error) {
//
//	// получаем информацию об аккаунте
//	accInfo := BinanceAccountInfo{}
//
//	values := url.Values{}
//	err := ex.SendAuthenticatedHTTPRequest(user, "GET", ACC_INFO, values, &accInfo)
//	if err != nil {
//		return nil, err
//	}
//
//	// сохраняем
//	balanceMap := make(map[string]memodels.Balance)
//	for _, v := range accInfo.Balances {
//		balanceMap[v.Asset] = memodels.Balance{
//			Available: v.Free,
//			OnOrders:  v.Locked,
//			Updated:   time.Now().Unix(),
//		}
//	}
//
//	return balanceMap, nil
//}
//
//// получение списка активных ордеров
//// на вход принимаем список пар для проверки. Для некоторыйх бирж он не важен
//// так как получают сразу по всем. А некоторые биржи - по очереди.
//func (ex *Binance) GetActiveOrders(user string, epairs []string) ([]interface{}, error) {
//	// получаем информацию об адресе
//	activeOrders := BinanceActiveOrders{}
//
//	values := url.Values{}
//	err := ex.SendAuthenticatedHTTPRequest(user, "GET", ACTIVE_ORDERS, values, &activeOrders)
//	if err != nil {
//		return nil, err
//	}
//	var ret []interface{}
//	for _, v := range activeOrders {
//		ret = append(ret, v.Symbol+":"+v.ClientOrderID)
//	}
//
//	return ret, nil
//}
//
//// Функция выставления ордера
//// orderType: "buy" or "sell"  (small)
//func (ex *Binance) Order(user, epair, orderType string, amount, price float64) (interface{}, error) {
//	newOrder := BinanceNewOrder{}
//	values := url.Values{}
//	values.Set("symbol", epair)
//	if orderType == constvar.ORDER_TYPE_BUY {
//		values.Set("side", "BUY")
//	} else {
//		values.Set("side", "SELL")
//	}
//	values.Set("type", "LIMIT")
//	values.Set("timeInForce", "GTC")
//	values.Set("quantity", strconv.FormatFloat(amount, 'f', -1, 64))
//	values.Set("price", strconv.FormatFloat(price, 'f', -1, 64))
//	values.Set("newOrderRespType", "ACK")
//	err := ex.SendAuthenticatedHTTPRequest(user, "POST", ORDER, values, &newOrder)
//	if err != nil {
//		return nil, err
//	}
//	//logs.Warn(newOrder)
//	return newOrder.Symbol + ":" + newOrder.ClientOrderID, nil
//}
//
//// функция отмены активного ордера
//func (ex *Binance) CancelOrder(user string, orderId interface{}) error {
//	cancelOrder := BinanceCancelOrder{}
//	values := url.Values{}
//	values.Set("symbol", common.SplitStrings(orderId.(string), ":")[0])
//	values.Set("origClientOrderId", common.SplitStrings(orderId.(string), ":")[1])
//	err := ex.SendAuthenticatedHTTPRequest(user, "DELETE", ORDER, values, &cancelOrder)
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//// функция проверки состояния биржи
//// по умолчанию - это проверка выдачи ордербуков
//// здесь - спец.функция
//func (ex *Binance) Online() bool {
//	// получаем информацию об аккаунте
//	status := BinanceSystemStatus{}
//	req := fmt.Sprintf("%s/%s", API_URL, SYSTEM_STATUS)
//	// ждем таймер
//	<-ex.getThrottle
//	err := ex.Performers.DoGet("share", req, true, &status)
//	if err != nil {
//		return false
//	}
//	if status.Status != 0 {
//		return false
//	}
//	return true
//}
//
//func (ex *Binance) SendAuthenticatedHTTPRequest(user, method, path string, values url.Values, result interface{}) error {
//	// ждем таймер
//	<-ex.postThrottle
//	headers := make(map[string]string)
//	headers["Content-Type"] = "application/x-www-form-urlencoded"
//	headers["X-MBX-APIKEY"] = ex.Performers.RKX.GetRKXK(user, ex.Name)
//	recvWindow := constvar.HTTP_POST_TIMEOUT.Nanoseconds() / 1000000 //делим на 1000 чтобы перевести в миллисекунды
//	values.Set("recvWindow", fmt.Sprint(recvWindow))                 //количество милисикунд, в течение которых биржа считает запрос валидным (с момента формирования запроса)
//	values.Set("timestamp", strconv.FormatInt(time.Now().Round(time.Millisecond).UnixNano()/(int64(time.Millisecond)/int64(time.Nanosecond)), 10))
//	hmac := common.GetHMAC(common.HASH_SHA256, []byte(values.Encode()), []byte(ex.Performers.RKX.GetRKXS(user, ex.Name)))
//
//	signature := common.HexEncodeToString(hmac)
//
//	response := ResponseError{}
//	resp := ""
//	err := errors.New("")
//	// Далее зависит от метода. Если GET то параметры отправляются в строке, если POST - в теле
//	if method == "GET" || path == BINANCE_WITHDRAWAL {
//		resp, err = ex.Performers.Do(user, ex.Name, method, API_URL+"/"+path+"?"+values.Encode()+"&signature="+signature, headers, url.Values{})
//		//resp, err = common.SendHTTPRequest(method, API_URL+"/"+path+"?"+values.Encode()+"&signature="+signature, headers, strings.NewReader(""))
//		if err != nil {
//			return err
//		}
//	} else if method == "POST" || method == "DELETE" {
//		values.Set("signature", signature)
//		resp, err = ex.Performers.Do(user, ex.Name, method, API_URL+"/"+path, headers, values)
//		//resp, err = common.SendHTTPRequest(method, API_URL+"/"+path, headers, strings.NewReader(values.Encode()))
//		if err != nil {
//			return err
//		}
//	} else {
//		return errors.New("Invalid method")
//	}
//
//	if resp[0] != '[' {
//		// если не выходе не массив - проверим на ResponseError
//		// массив не проверяем, так как это уже означает, что ошибки нет. А демаршалить массив в ResponseError мы все равно без ошибки не сможем
//		err = common.JSONDecode([]byte(resp), &response)
//		if err != nil {
//			return err
//		}
//
//		// если ответ отрицательный
//		if response.Code != 0 {
//			return errors.New(response.Msg)
//		}
//	}
//
//	// иначе демаршалим в нужный интерфейс
//	err = common.JSONDecode([]byte(resp), &result)
//	if err != nil {
//		return err
//	}
//
//	return nil
//}
